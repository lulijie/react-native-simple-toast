require 'json'

package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name                   = 'RCTToast'
  s.version                = package['version']
  s.summary                = package['description']
  s.description            = package['description']
  s.homepage               = 'https://bitbucket.org/lulijie/react-native-simple-toast'
  s.license                = package['license']
  s.author                 = package['author']
  s.source                 = { :git => 'https://bitbucket.org/lulijie/react-native-simple-toast.git', :tag => s.version }

  s.platform               = :ios, '8.0'
  s.ios.deployment_target  = '8.0'

  s.preserve_paths         = 'LICENSE', 'package.json'
  s.source_files           = '**/*.{h,m}'
  s.dependency               'React'
end
